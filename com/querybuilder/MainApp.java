package com.querybuilder;

public class MainApp {
	public static void main(String args[]) {
		SelectAndUpdateQueryWarpper selectAndUpdateQueryWarpper = new SelectAndUpdateQueryWarpper();
		
		QueryBuilder selectQueryBuilder = new SelectQueryBuiler();
	//	QueryBuilder updateQueryBuilder = new UpdateQueryBuilder();
		
		selectAndUpdateQueryWarpper.setQueryBuilder(selectQueryBuilder);
		selectAndUpdateQueryWarpper.getBuilder().createQuery();
		selectAndUpdateQueryWarpper.getBuilder().addSubQuery("Select * from");
		//selectAndUpdateQueryWarpper.getBuilder().columnName("title");
		selectAndUpdateQueryWarpper.getBuilder().tableFrom("titles");
		
		//selectAndUpdateQueryWarpper.getBuilder().addWhere();
		
		
		Query query = selectAndUpdateQueryWarpper.getBuilder().query;
		
		System.out.println(" Select Query is " + query);
		
		//================================================
	/*			
		selectAndUpdateQueryWarpper.setQueryBuilder(updateQueryBuilder);
		selectAndUpdateQueryWarpper.constructQuery();
		
		Query query1 = selectAndUpdateQueryWarpper.getQuery();
		
		System.out.println(" Update Query is " + query1);*/
	}
}
