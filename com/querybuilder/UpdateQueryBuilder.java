package com.querybuilder;

public class UpdateQueryBuilder extends QueryBuilder{

	
//	Query query;
	
	public void addWhere() {
		query.setWhere("where");
		
	}
	@Override
	public void tableFrom(String queryTable) {
		query.setFromTable(queryTable);
		
	}
	

	@Override
	public Query getQuery() {
		
		return new Query();
	}
	@Override
	public void addSubQuery(String subQuery) {
		query.setSubQuery(subQuery);
		
	}
	@Override
	public void columnName(String colmnName) {
		query.setColumnName(colmnName);
		
	}
	@Override
	public void limit(String lmt) {
		query.setLimit(lmt);
		
	}

}
