package com.querybuilder;

public class SelectQueryBuiler extends QueryBuilder{
	public void addWhere(){
		query.setWhere("where");
		
	}
	public void tableFrom(String queryTable) {
		query.setFromTable(queryTable);
		
	}
	public Query getQuery() {
		return new Query();
	}
	public void addSubQuery(String subQuery) {
		query.setSubQuery(subQuery);
	}
	
	public void columnName(String colmn) {
		query.setColumnName(colmn);
		
	}
	
	
	
	public void limit(String limit) {
		query.setLimit(limit);
		
	}
}
