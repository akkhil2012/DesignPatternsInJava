package com.querybuilder;

public class SelectAndUpdateQueryWarpper {

	private QueryBuilder builder;
	
	//private String subQuery;
	//private String subQuery;
	
	public void setQueryBuilder(QueryBuilder queryBuilder){
		builder = queryBuilder;
	}
	
	
	public QueryBuilder getBuilder() {
		return builder;
	}


	public Query getQuery(){
		return builder.query;
	}
	
	
	/*public void constructQuery(){
		builder.createQuery();
		builder.addSubQuery(subQuery);
		builder.columnName();
	}*/
	
}
