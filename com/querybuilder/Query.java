package com.querybuilder;

public  class Query {


	private String where = "";
	


	private String subQuery = "";
	
	private String ColumnName = "";
	
	private String limit = "";
	
/*	public void setSetSubQuery(String setSubQuery) {
		this.setSubQuery = setSubQuery;
	}

	private String setSubQuery = "";
*/	
	public void setSubQuery(String selectSubQuery) {
		this.subQuery = selectSubQuery;
	}

	public String getWhere() {
		return where;
	}

	public String getSubQuery() {
		return subQuery;
	}

	public String getFromTable() {
		return fromTable;
	}

	private String fromTable = "";
	
	public void setWhere(String where) {
		this.where = where;
	}

	public void setFromTable(String fromTable) {
		this.fromTable = fromTable;
	}
	
	public void setColumnName(String columnName) {
		ColumnName = columnName;
	}


	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}
	
	public String toString() {
		return this.subQuery +" "+this.fromTable+" "+ this.where+ this.ColumnName;
	}

	
}
