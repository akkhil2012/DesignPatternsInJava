package com.querybuilder;

public abstract class QueryBuilder {
	
	  public Query query;
	

	 // public abstract String buildQuery();
	  public abstract void addWhere();
	  public abstract void tableFrom(String table);
	  public abstract void addSubQuery(String subQuery);
	  public abstract void columnName(String column);
	  public abstract void limit(String limit);
	
	  public  void createQuery(){
		  query = new Query();
	  }
	
	  public abstract Query getQuery();
	
}
