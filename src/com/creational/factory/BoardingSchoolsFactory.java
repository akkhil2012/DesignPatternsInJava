package com.creational.factory;

public class BoardingSchoolsFactory implements BoardingSchools{
	public BoardingSchools createSchools(String schoolType) {
		if(schoolType.equals("govt"))
			return new GovtFundedSchools();
		else if(schoolType.equals("private"))
			return new PrivateRunSchools();
		else if(schoolType.equals("ngo"))
			return new NGOFundedSchools();
		return null;
	}

}
