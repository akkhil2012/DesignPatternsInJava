package com.creational.factory;

public interface BoardingSchools {
	
	BoardingSchools createSchools(String schoolType);

}
