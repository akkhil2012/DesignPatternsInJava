package com.creational.factory;

public class MainApp {
	public static void main(String args[]) {
		BoardingSchoolsFactory factory = new BoardingSchoolsFactory();
		factory.createSchools("govt");
		factory.createSchools("private");
		factory.createSchools("ngo");
	}
}
