package com.creational.builder;

public class Contractor {

	private SchoolBuilder schoolBuilder;
	
	public void constructSchool(){
		
		schoolBuilder.acedemicsBlock();
		schoolBuilder.buildCommonHall();
	}
	
	
}
