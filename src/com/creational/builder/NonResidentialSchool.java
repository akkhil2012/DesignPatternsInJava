package com.creational.builder;

public class NonResidentialSchool extends SchoolBuilder{

    SchoolBuilding schoolBuilding;
	public void acedemicsBlock() {
		schoolBuilding.createAcademicBlock("NON Residential school Academics block ");
		
	}

	@Override
	public void buildCommonHall() {
		schoolBuilding.buildCommonHall("NON Residential school CommonHall");
		
	}

}
