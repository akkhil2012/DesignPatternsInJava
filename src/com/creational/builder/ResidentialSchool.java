package com.creational.builder;

public class ResidentialSchool extends SchoolBuilder{

	SchoolBuilding schoolBuilding;
	
	public void acedemicsBlock() {
		schoolBuilding.createAcademicBlock("Residential School Building");
		
	}

	@Override
	public void buildCommonHall() {
		schoolBuilding.buildCommonHall("Residential school Common Hall");
		
	}
	
	

}
