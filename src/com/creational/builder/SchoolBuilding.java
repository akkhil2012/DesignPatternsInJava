package com.creational.builder;

public class SchoolBuilding {
	
	public void createAcademicBlock(String block){
		System.out.println(block);
	}
	
	public void buildCommonHall(String hall){
		System.out.println(hall);
	}

}
