package com.behavioural.state;

public interface ClaimState {
	
	void claimStatus(Claim claim);
	
	void changeClaimState(Claim claim);

}
