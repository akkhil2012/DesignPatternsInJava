package com.behavioural.state;

public class Claim {
	
	
	private ClaimState claimState;

	
	
	
	
	
	public Claim() {
		claimState = new ClaimInitialized();
	}
	
	
	public void setClaimState(ClaimState s){
		claimState = s;
	}
	
	
	public void getClaimStatus(){
		claimState.claimStatus(this);
	}
	
	public ClaimState getClaimState() {
		return claimState;
	}
/*
	public void setClaimState(ClaimState claimState) {
		this.claimState = claimState;
	}

	public ClaimState getClaimState() {
		return claimState;
	}
	
	
	public void initializaClaim(){
	
		claimState.claimStatus();
		
		if(claimState instanceof ClaimInitialized){
			
			ClaimState claimState = new ClaimInProgress();
			setClaimState(claimState);
			System.out.println(" Claim is in Progress ");
		}
		
		
	}
	
	
	public void claimInProgress(){
		
		claimState.claimStatus();
		
		if(claimState instanceof ClaimInProgress){
			
			ClaimState claimState = new ClaimResult();
			setClaimState(claimState);
			System.out.println(" Claim Settlement Done");
		}
		
	}
	
	
	
	
	public void claimSettled() {
		
		claimState.claimStatus();
		System.out.println(" Claim settlement done");
	}
	
	
	
	
	
	
	
	
	
*/
}
