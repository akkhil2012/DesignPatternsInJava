package com.behavioural.state;

public class ClaimInProgress implements ClaimState{

   ClaimState claimState;
	
	public ClaimInProgress(){
	//	this.claimState = claimState;
	}
	
	
	@Override
	public void claimStatus(Claim claim) {
		System.out.println(" Claim in progress ");
		changeClaimState(claim);
	}


	@Override
	public void changeClaimState(Claim claim) {
		claim.setClaimState(new ClaimResult());
		
	}

}
