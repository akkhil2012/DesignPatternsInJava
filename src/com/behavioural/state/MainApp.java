package com.behavioural.state;



public class MainApp {
	public static void main(String args[]) {
		Claim claim = new Claim();
		ClaimState claimState = claim.getClaimState();
		claimState.claimStatus(claim);
		
		claimState = claim.getClaimState();
		claimState.claimStatus(claim);
		
		
		claimState = claim.getClaimState();
		claimState.claimStatus(claim);
	}
}
