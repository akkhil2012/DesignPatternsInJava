package com.behavioural.state;

public class ClaimInitialized implements ClaimState{

	Claim claim;
	
	
	
	public ClaimInitialized(){
		//this.claimState = claimState;
	}
	
	
	public void claimStatus(Claim claim) {
		System.out.println(" Claim Initialized....");
		changeClaimState(claim);
	}


	@Override
	public void changeClaimState(Claim claim) {
		claim.setClaimState(new ClaimInProgress());
		
	}


	
	/*public void changeClaimState() {
		claim.setClaimState(new ClaimInProgress());
		
	}*/



}
