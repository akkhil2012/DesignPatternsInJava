package com.behavioural.state;

public class ClaimResult implements ClaimState{

   ClaimState claimState;
	
	public ClaimResult(){
	//	this.claimState = claimState;
	}

	
	@Override
	public void claimStatus(Claim claim) {
		System.out.println(" Claim Result ");
		changeClaimState(claim);
		
	}

	@Override
	public void changeClaimState(Claim claim) {
		System.out.println(" Claim process completed");
		
	}

}
