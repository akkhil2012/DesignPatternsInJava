package com.behavioural.iterator;

public class SongInfo {
	
	
	String songName;
	String singer;
	
	public SongInfo(String songName, String singer) {
	
		this.songName = songName;
		this.singer = singer;
	}

}
