package com.behavioural.iterator;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class SongOf90s implements SongsIterator{

	SongInfo songInfo;
	
	int songKey = 0;
	
	Hashtable<Integer,SongInfo> songInfoTable = new Hashtable<Integer,SongInfo>();
	
	public void add90Songs(SongInfo songInfo){
		
		  songInfoTable.put(songKey, songInfo);
		  songKey++;
	}

	@Override
	public Iterator<SongInfo> createIterator() {
		
		return songInfoTable.values().iterator();
	}
	
}
