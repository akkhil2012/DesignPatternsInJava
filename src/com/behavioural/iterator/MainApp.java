package com.behavioural.iterator;

import java.util.Iterator;

public class MainApp {
	
	
	public static void main(String[] args) {
		
		SongOf70s songOf70s = new SongOf70s();
		SongOf80S songOf80s = new SongOf80S();
		SongOf90s songOf90s = new SongOf90s();
		
		SongInfo songInfo = new SongInfo("firstNight", "John Lenon");
		
		//songOf70s.add70Songs(songInfo);
		songOf70s.add70Songs(new SongInfo("jason", "Mr X"));
		songOf70s.add70Songs(new SongInfo("john", "MJ"));
		
		
         songInfo = new SongInfo("firstDay", "John Lenon New Era");
		
        // songOf80s.add80Songs(songInfo);
         songOf80s.add80Songs(new SongInfo("jason New Era", "Mr X New Era"));
         songOf80s.add80Songs(new SongInfo("john New Era", "MJ New Era"));
		
		
        songInfo = new SongInfo("firstEvening", "John Lenon Evening");
		
		//songOf90s.add90Songs(songInfo);
		songOf90s.add90Songs(new SongInfo("jason New Evening", "Mr X Evening"));
		songOf90s.add90Songs(new SongInfo("john New Evening", "MJ Evening"));
		
		
		
		Iterator<SongInfo> songs70Iterator = (Iterator<SongInfo>)songOf70s.createIterator();
		Iterator<SongInfo> songs80Iterator = (Iterator<SongInfo>)songOf80s.createIterator();
		Iterator<SongInfo> songs90Iterator = (Iterator<SongInfo>)songOf90s.createIterator();
		
		iterateSongs(songs70Iterator);
		iterateSongs(songs80Iterator);
		iterateSongs(songs90Iterator);
		
		
	}

	
	private static void iterateSongs(Iterator<SongInfo> ite){
		
		while(ite.hasNext()){
			System.out.print(" "+ite.next().songName + ite.next().singer);
		}
		System.out.println(" ");
		
	}
	
	
	
	
	
}
