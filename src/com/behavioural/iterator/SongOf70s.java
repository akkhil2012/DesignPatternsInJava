package com.behavioural.iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class SongOf70s implements SongsIterator{
	
	SongInfo songInfo;
	
	ArrayList<SongInfo> songArrayList = new ArrayList<SongInfo>();
	
	public void add70Songs(SongInfo songInfo){
		
		  songArrayList.add(songInfo);
		  
	}

	@Override
	public Iterator<SongInfo> createIterator() {
		
		return songArrayList.iterator();
	}

}
