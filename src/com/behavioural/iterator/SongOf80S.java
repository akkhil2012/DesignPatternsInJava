package com.behavioural.iterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
/*
 * 
 * Since Iterator is Only for the Collections
 */
public class SongOf80S implements SongsIterator{

	SongInfo songInfo;
	
	SongInfo[] songArray = new SongInfo[2];
	
	int i = 0;
	
	public void add80Songs(SongInfo songInfo){
		
		 songArray[i++]=songInfo;
		 
	}

	@Override
	public Iterator<SongInfo> createIterator() {
		// TODO Auto-generated method stub
		return Arrays.asList(songArray).iterator();
	}
}
