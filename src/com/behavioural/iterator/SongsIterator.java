package com.behavioural.iterator;

import java.util.Iterator;

public interface SongsIterator{
	
	Iterator<SongInfo> createIterator();

}
