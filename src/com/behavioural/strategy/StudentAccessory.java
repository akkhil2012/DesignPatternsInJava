package com.behavioural.strategy;

public class StudentAccessory {

	Accessory accessory;
	
	public void getAccesoriesProvided(Accessory accessory){
		accessory.getAccessory();
	}
}
