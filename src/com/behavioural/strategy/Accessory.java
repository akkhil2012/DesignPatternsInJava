package com.behavioural.strategy;

public interface Accessory {
	void getAccessory();
}
