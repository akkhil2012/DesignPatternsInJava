package com.behavioural.strategy;

public class MainApp {

	
	public static void main(String args[]) {
		StudentAccessory studentAccessory = new StudentAccessory();
		
		System.out.println(" Get accessories provided to Govet School students");
		studentAccessory.getAccesoriesProvided(new Uniform());
		
		System.out.println(" ");
		
        System.out.println(" Get accessories provided to NGOFunded School students");
        studentAccessory.getAccesoriesProvided(new Uniform());
        studentAccessory.getAccesoriesProvided(new Books());
        
        System.out.println(" ");
		
        System.out.println(" Get accessories provided to Private School students");
        studentAccessory.getAccesoriesProvided(new Books());
	}
	
}
