package com.behavioural.observer;

public interface SensorSystemInterface {
	public void addObserver(ObserverInterface observer);
	public void remoiveObserver(ObserverInterface observer);
	public void startAlarm();
}
