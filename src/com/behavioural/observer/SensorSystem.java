package com.behavioural.observer;

import java.util.ArrayList;

public class SensorSystem implements SensorSystemInterface {

	ArrayList<ObserverInterface> obsList = new ArrayList<ObserverInterface>();

	public void addObserver(ObserverInterface observerInterface) {
		System.out.println(" Added Observer " + observerInterface.getClass().getSimpleName());
		obsList.add(observerInterface);

	}

	public void remoiveObserver(ObserverInterface observer) {
		System.out.println(" Removed Observer " + observer.getClass().getSimpleName());
		obsList.remove(observer);

	}

	public void startAlarm() {
		for (ObserverInterface ob : obsList) {
			ob.beepAlarm();
		}

	}

}
