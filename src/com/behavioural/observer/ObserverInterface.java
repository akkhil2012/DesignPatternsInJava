package com.behavioural.observer;

public interface ObserverInterface {
	public void beepAlarm();
}
