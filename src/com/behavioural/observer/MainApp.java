package com.behavioural.observer;

public class MainApp {
	public static void main(String args[]){
		SensorSystemInterface systemInterface = new SensorSystem();
		systemInterface.addObserver(new Gate());
		systemInterface.addObserver(new Survelience());
		systemInterface.addObserver(new Lighting());
		
		systemInterface.startAlarm();
		
		systemInterface.remoiveObserver(new Gate());
	}

}
