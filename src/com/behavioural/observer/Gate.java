package com.behavioural.observer;

public class Gate implements ObserverInterface{

	@Override
	public void beepAlarm() {
		System.out.println(" Close the Gate");
		
	}

}
